package collections;

import java.util.*;

public class collectionsDemo {

    public static void main(String[] args) {

        ArrayList<Integer> arrayList = new ArrayList<>();

        System.out.println("arrayList = " + arrayList);
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(4);
        arrayList.add(6);
        arrayList.add(2);
        arrayList.add(10);
        System.out.println("arrayList = " + arrayList);

        HashSet<Integer> hashSet = new HashSet<>();

        System.out.println("hashSet = " + hashSet);
        hashSet.add(1);
        hashSet.add(2);
        hashSet.add(4);
        hashSet.add(6);
        hashSet.add(2);
        hashSet.add(10);
        System.out.println("hashSet = " + hashSet);

        TreeSet<String> treeSet = new TreeSet<>();

        treeSet.add("First");
        treeSet.add("Second");
        treeSet.add("Third");
        treeSet.add("fourth");

        System.out.println("treeSet = " + treeSet);

        HashMap<String,Object> map = new HashMap<>();
        map.put("Name","Ahmed Alkaff");
        map.put("Email","a.Alkaff@sdkjordan.com");
        map.put("Phone","07889462541");
        map.put("Email2","a.Alkaff@sdkjordan.org");
        System.out.println("map = " + map);

        LinkedList<Map<String,Object>> contacts = new LinkedList<>();

        contacts.add(map);


        HashMap<String,Object> map1 = new HashMap<>();
        map1.put("Name","Mosab Malkawi");
        map1.put("Email","m.malkawi@sdkjordan.com");
        map1.put("Phone","0789888400");

        contacts.add(map1);

        System.out.println("contacts = " + contacts);

        for( Map<String,Object> m : contacts)
        {
            if(m.containsKey("Email2"))
                System.out.println(m.get("Email2"));
        }


    }
}

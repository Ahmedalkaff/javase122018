package oop;

public class AbstractDemo {

    // abstract keyword can be used with

    // 1) instance methods : abstract method is a method without any body
    // Abstract methods in the super class must be implemented by any concrete (non abstract sub-class) sub-class

    // 2) classes : is a class that may have an abstract methods

    //


    static abstract class  Shape {

        public Shape ()
        {
            System.out.println("Shape()");
        }
        int x ;
        abstract  public  void method();

    }


    static class Rectangle  extends  Shape
    {
        @Override
        public void method() {
            System.out.println("Rectangle:method()");
        }
    }

    static class Triangle extends Shape {

        @Override
        public void method() {

        }

    }
    public static void main(String[] args) {

//         Shape s = new Shape();
//         s.method();

        Shape s = new Rectangle();      // this will create an object of shape them an object of Rectangle
        s.method();         // Rectangle:method()


        // anonymous inner type
        Shape s1 = new Shape() {
            @Override
            public void method() {
                System.out.println("Anonymous:method()");
            }
        } ;

        s1.method();            // Anonymous:method()


    }

}

package oop;

import oop.clasess.Point;

import java.util.Locale;

public class DataHiddenDemo {


    public static void main(String[] args) {


        Point p = new Point();   // instead of  int x , y , z ;
        p.x = 1 ;
        p.y = 2 ;
        p.z = 3 ;



        Point p1 = new Point();        // instead of    int x1 , y1 , z1 ;

        p1.x = 4 ;
        p1.y = 5 ;
        p1.z = 6 ;

        System.out.println("P  = " + printPoint(p));
        System.out.println("P1 = " + printPoint(p1));



//        System.out.printf(Locale.getDefault(),"P  {%d, %d, %d}%n",p.x,p.y,p.z);
//        System.out.printf(Locale.getDefault(),"P1 {%d, %d, %d}%n",p1.x,p1.y,p1.z);
    }

    public  static String printPoint(Point p)
    {
       return  String.format(Locale.getDefault(),"Point  {%d, %d, %d}",p.x,p.y,p.z);
    }
}

package oop;

public class EncapsulationDemo {

    // encapsulation
    // access modifiers 1) public 2) protected 3)no modifier -package- 4)private
    /*              | in side the class | inside any other class  | inside any class outside | inside child class       | inside child outside    |
                    |                   | in the same package     |  outside the  package    | in the same package      |  outside the  package   |
                    -------------------------------------------------------------------------------------------------------------------------------
        public      |       Yes         |           Yes           |            Yes           |         Yes              |           Yes           |                   |
        protected   |       Yes         |           Yes           |            NO            |         Yes              |           Yes           |                   |
        PACKAGE     |       Yes         |           Yes           |            NO            |         Yes              |           NO            |                   |
        private     |       Yes         |           NO            |            NO            |         NO               |           NO            |                   |

     */


    public static void main(String[] args) {

    }
}

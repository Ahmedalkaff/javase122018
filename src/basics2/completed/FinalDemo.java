package basics2.completed;

public class FinalDemo {
    // final keyword used with
    // 1) variables : allows only one initialization
    // 2) methods : can NOT be overridden (in OOP)
    // 3) classes : can NOT be inherited (in OOP)

    static final  int Z = 10 ;   // instance (non static)/ class variable (static) are automatically  initialized
    final int[] arr  ={1,2,3};

    public static void main(String[] args) {

        final  int x = 10 ;
        final  int y ;          //TODO: local variable are NOT initialized automatically, why ?

        y = 10 ;
        // x = 20 ;    // not allowed !

    }
}

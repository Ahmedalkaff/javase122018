package basics2.completed;

public class InnerClassesDemo {

    public static void main(String[] args) {

        OuterClass.StaticInnerClass staticInnerClass =
                new OuterClass.StaticInnerClass();

//        OuterClass.InnerClass innerClass =
//                new OuterClass.InnerClass();

        OuterClass outerClass = new OuterClass();

        OuterClass.InnerClass innerClass =
                outerClass.new InnerClass();
        /// OR

        OuterClass.InnerClass innerClass1 =
                new OuterClass().new InnerClass();

    }

}

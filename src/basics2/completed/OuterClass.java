package basics2.completed;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class OuterClass {

    // Inner class and outerclass can access each other's private members if that is possible
    private static int staticValue ;

    private  int nonStaticValue ;

    // this class will be created inside each object of OuterClass
    class InnerClass {

        // you can't create static field inside non static inner class
//        static  int a ;
       private int a ;
        public  void methd()
        {
            staticValue = 10 ;
            nonStaticValue = 10 ;
        }

//        static  public  void staticMethod()
//        {
//            // can't create static method inside non static inner class
//        }
    }
    // this class will be created inside the OuterClass class
    static class StaticInnerClass {

        // you can't reach the static members of the outer class from static inner class
        private  int b ;
        private static    int c ;
        public  void methd()
        {
            staticValue = 10 ;
            // nonStaticValue = 10 ;
        }

        static  public  void staticMethod()
        {
            staticValue = 10 ;
            //nonStaticValue = 10 ;
        }
    }


    public  void methd()
    {
        InnerClass innerClass = new InnerClass();
        innerClass.a = 10 ;

        StaticInnerClass staticInnerClass = new StaticInnerClass();
        staticInnerClass.b  = 20 ;


        StaticInnerClass.c = 10 ;

    }

    static  public  void staticMethod()
    {
        // in the static method of the outerClass , there is no object of the outclass
        // so, there is no InnerClass is exist
//        InnerClass innerClass = new InnerClass();
//        innerClass.a = 10 ;

        StaticInnerClass staticInnerClass = new StaticInnerClass();
        staticInnerClass.b  = 20 ;


        StaticInnerClass.c = 10 ;
    }
}

package tasks;

import java.util.Scanner;

public class PrimeNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("N:");
        int n = scanner.nextInt();

        if (n <= 0) {
            System.out.println("No prime number less than or equal to " + n);
        } else {
            int sqroot;
            boolean isPrime;
            for (int i = 1; i <= n; i++) {
                if (i <= 3)
                    System.out.println(i);
                else {
                    isPrime = true;
                    sqroot = (int) Math.sqrt(i);
                    for (int j = 2; j <= sqroot; j++) {
                        if (i % j == 0) {
                            isPrime = false;
                            break;
                        }
                    }
                    if (isPrime)
                        System.out.println(i);
                }
            }
        }
        scanner.close();
    }
}

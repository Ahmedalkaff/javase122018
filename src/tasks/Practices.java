package tasks;

public class Practices {

    public static void main(String[] args) {
        int a = 34;             // 34
        int b = 21;             // 20
        int c = a++ + ++b;       //  55
        int d = --a + --b + c--;    //34 + 21 + 56;     // 111
        int e = a + +b + +c + d--;      // 34 + 21 + 55 + 111;  // 221
        int f = -a + b-- + -c - d++;        // -34 + 21 -55 - 110; // -178
        int sum = a + b + c + d + e + f;
        System.out.println("sum = " + sum);

    }
}

package tasks.report;

public interface Condition<T> {
    boolean test(T t);
}

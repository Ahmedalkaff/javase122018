package tasks;

import java.util.Locale;

public class Swap {
    public static void main(String[] args) {
        int a = 2147483647;
        int b = 100000;
        System.out.printf(Locale.getDefault(), "a = %12d , b = %12d%n", a, b);
        a = a + b;
        System.out.printf(Locale.getDefault(), "a = %12d , b = %12d%n", a, b);
        b = a - b;      // b  =  9 - 5 ;
        System.out.printf(Locale.getDefault(), "a = %12d , b = %12d%n", a, b);
        a = a - b;      // a  = 9 - 5 ;
       System.out.printf(Locale.getDefault(), "a = %10d , b = %12d%n", a, b);
        System.out.println("=========================");
//
//        System.out.printf(Locale.getDefault(), "a = %10d , b = %10d%n", a, b);
//        a = a * b;
//        System.out.printf(Locale.getDefault(), "a = %10d , b = %10d%n", a, b);
//        b = a / b;      // b  =  9 - 5 ;
//        System.out.printf(Locale.getDefault(), "a = %10d , b = %10d%n", a, b);
//        a = a / b;      // a  = 9 - 5 ;
//        System.out.printf(Locale.getDefault(), "a = %10d , b = %10d%n", a, b);
//        System.out.println("=========================");

        System.out.printf(Locale.getDefault(), "a = %10d , b = %10d%n", a, b);
        a = a ^ b;
        System.out.printf(Locale.getDefault(), "a = %10d , b = %10d%n", a, b);
        b = a ^ b;      // b  =  9 - 5 ;
        System.out.printf(Locale.getDefault(), "a = %10d , b = %10d%n", a, b);
        a = a ^ b;      // a  = 9 - 5 ;
        System.out.printf(Locale.getDefault(), "a = %10d , b = %10d%n", a, b);

    }
}
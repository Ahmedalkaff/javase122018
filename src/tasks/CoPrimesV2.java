package tasks;

import java.util.Locale;
import java.util.Scanner;

public class CoPrimesV2 {
    // TODO: 1) the user is prompted to enter a positive integer n, 0 < n <= 10
    // TODO: 2) Promote the user to enter n integer number
    // TODO: 3) test if all the n numbers are co-primes  , co-prime numbers must have gcd(x,y) = 1 for all x, y in the n numbers


    // TODO: 4) rewrite this solution using methods

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("N:");
        int n = scanner.nextInt();          /// 5
        if (n <= 0 || n > 10) {
            System.out.println("Invalid n value:" + n);
        } else {
            int[] a = new int[n];
            for (int i = 0; i < a.length; i++) {      // 1
                System.out.printf(Locale.getDefault(), "The integer number [%d]:", i);
                a[i] = scanner.nextInt();

            }

            int max = 0, min = 0, gcd = 1;
            boolean isCoPrime = true;
            System.out.printf(Locale.getDefault(), "The integer number are:");

            for (int i = 1; i < a.length; i++) {
                for (int m = i - 1; m >= 0; m--) {
                    gcd = 1;
                    max = a[i] > a[m] ? a[i] : a[m];
                    min = a[i] > a[m] ? a[m] : a[i];
                    for (int j = min; j >= 1; --j) {
                        if (max % j == 0 && min % j == 0) {
                            gcd = j;
                            break;
                        }
                    }
                    isCoPrime = (gcd == 1);
                    if (!isCoPrime)
                        break;
                }

            }

            if (isCoPrime)
                System.out.println(", this group are co-primes");
            else
                System.out.println(", this group are NOT co-primes");
            System.out.println("\b\b\n");
        }
        scanner.close();
    }


}

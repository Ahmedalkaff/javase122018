package tasks;

import java.util.Locale;

public class Tester {

    public static void main(String[] args) {
        int size = 10;
        int[][] arr = new int[size][size];
        int c = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i == j || (size - i-1 == j))
                    arr[i][j] = 1;
                else
                    arr[i][j] = 0;
                System.out.printf(Locale.getDefault(), "%5d,", arr[i][j]);
            }
            System.out.println();
        }

        for (int i = 0; i < arr.length; i++)
            System.out.println(arr[i][i]);
    }
}

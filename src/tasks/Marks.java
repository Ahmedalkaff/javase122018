package tasks;

import java.util.Locale;
import java.util.Scanner;

public class Marks {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);

        double mark;
        System.out.print("Enter your mark:");
        mark =scanner.nextDouble() ;

        if(mark <35 || mark >100)
            System.err.println("Error");
        else if(mark < 50)
            System.out.println("Failed");
        else if(mark < 68)
            System.out.println("Passed");
        else if(mark < 76)
            System.out.println("Good");
        else if(mark < 84)
            System.out.println("Very good");
        else
            System.out.println("Excellent");

        System.out.println(mark <35 || mark >100 ?"Error" : mark < 50 ? "Failed" : mark<68 ? "Passed":mark < 76 ? "Good" : mark<84 ? "Very good":"Excellent");
    }
}

package basics1.completed;

import java.util.Locale;
import java.util.Random;

public class MoreAboutMethods {

    private static String printArray(int[] a) {

        StringBuffer buffer = new StringBuffer("{");
        for(int i=0;i<a.length;i++) {
            buffer.append(a[i]).append(i== a.length-1 ?"}":", ");
        }
        return  buffer.toString();
    }


    private static void fillArray(int[] arr , int bound1, int bound2)
    {
        int min = bound1 < bound2 ? bound1 : bound2 ;           // 35
        int max = bound1 < bound2 ? bound2 : bound1 ;            // 100
        int gap = max - min + 1;            // 66
        Random random = new Random();
        for(int i=0;i<arr.length;i++) {
            arr[i] = min + random.nextInt(gap) ;
        }
    }

    private static int findMin(int[] arr )
    {
        int min = Integer.MAX_VALUE ;
        for(int i=0;i<arr.length;i++) {
          if(arr[i] < min)
              min = arr[i];
        }

        return  min ;
    }

    private static int findMax(int[] arr )
    {
        int max = Integer.MIN_VALUE ;
        for(int i=0;i<arr.length;i++) {
            if(arr[i] > max)
                max = arr[i];
        }
        return  max ;
    }

    // TODO: build one method that return both of the minimum and the maximum of an array

    // To solve this in Java we have 2 options
    // Option 1: The bad one because it will work if all return variables are from the same type and there is no way to tell which return variables is which
// in this case user of this method can't tell which is the min and which is the max without testing the values
    private static int[] findMinMax(int[] arr )
    {
        int[] minmax = {Integer.MAX_VALUE,  Integer.MIN_VALUE };
        for(int i=0;i<arr.length;i++) {
            if(arr[i] > minmax[1])
                minmax[1] = arr[i];
            if(arr[i] < minmax[0])
                minmax[0] = arr[i] ;
        }
        return  minmax ;
    }

    // Option 2:
    static  class  MinMax {
        int min = Integer.MAX_VALUE ;
        int max = Integer.MIN_VALUE ;
    }
    private static MinMax findMinMax2(int[] arr )
    {
        MinMax result = new MinMax();
        for(int i=0;i<arr.length;i++) {
            if(arr[i] > result.max)
                result.max = arr[i];
            if(arr[i] < result.min)
                result.min = arr[i] ;
        }
        return  result ;
    }


    public static void main(String[] args) {

        int[] arr = new int[30];
        fillArray(arr,35,100);
        System.out.println("  printArray(arr) = " +   printArray(arr));

        System.out.println("Min = " + findMin(arr));
        System.out.println("Max = " + findMax(arr));

        int[] result = findMinMax(arr);
        System.out.printf(Locale.getDefault(),"The min: %d , max: %d%n",result[0],result[1]);

        MinMax rest = findMinMax2(arr);
        System.out.printf(Locale.getDefault(),"The min: %d , max: %d%n",rest.min,rest.max);

    }
}

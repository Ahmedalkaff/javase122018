package basics1.completed;

public class Strings {

    // string : is set of characters (array of characters )

    public static void main(String[] args) {

        String str = "string";
        String str1 = "String";
        String string = new String("String");
        String string1 = new String("String");

        System.out.println("str    = " + str);
        System.out.println("string = " + string);

        System.out.println("(str == str1) = " + (str == str1));             // true , false , false
        System.out.println("(string == string1) = " + (string == string1)); // true , true , false
        System.out.println("(str == string) = " + (str == string));         // true , false , false

        System.out.println("======== equals() ========");
        System.out.println("str.equals(str1) = " + str.equals(str1));
        System.out.println("string = " + string.equals(string1));
        System.out.println("str = " + str.equals(string));

        System.out.println("======== equalsIgnoreCase() ========");
        System.out.println("str.equals(str1) = " + str.equalsIgnoreCase(str1));
        System.out.println("string = " + string.equalsIgnoreCase(string1));
        System.out.println("str = " + str.equalsIgnoreCase(string));
        String text = "this is the original text with number 15 25 13.5 and 85";

        System.out.println(text.replaceAll("([^\\d\\s]+)","*"));
        // ****Important NOTE: no string method change on the original string, all return new string
        System.out.println("text = " + text);

        String [] spliter = text.split("(.){1,1}");
        for(String s: spliter) {
            System.out.println(s);
        }
        // OR using regular for loop

        /// Split by characters

        char ch ;
        for(int i=0;i<text.length();i++)
        {
            ch = text.charAt(i);
            if(Character.isAlphabetic(ch) || Character.isDigit(ch) || Character.isSpaceChar(ch))
                System.out.println(ch);
        }

        System.out.println();

    }
}

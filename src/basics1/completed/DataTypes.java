package basics1.completed;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;

public class DataTypes {
    public static void main(String[] args) {

        boolean b = true;

        System.out.println("b = " + b);
        byte a = -128;// -128 to 127

        short s = -32768;

        int i = -2147483648;

        long l = -9223372036854775808L;     // any number is represented as int,
        // long number that exceeds the integer range must be suffixed with L/l

        // if you need more than long use BigInteger

        System.out.printf(Locale.getDefault(),"l = %,d%n" , l);
        BigInteger bi = new BigInteger("92233720368547758080054854545165865896585966565666656859600");

        System.out.println("bi = " + bi);

        // any floating point number is represented as double by default,
        // so any float number must be suffixed with f/F
        float f = 152.92233720368547758080054854545165865896585966565666656859600F;            // upto 6 decimal places
        double d = 152.92233720368547758080054854545165865896585966565666656859600;            // upto 13 decimal places

        // if you need more precession  use BigDecimal
        BigDecimal bigDecimal = new BigDecimal("152.92233720368547758080054854545165865896585966565666656859600");
        System.out.println("f = " + f);
        System.out.println("d = " + d);
        System.out.println("bigDecimal = " + bigDecimal);


        //Hello.main(args);



    }
}

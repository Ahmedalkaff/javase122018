package basics1.completed;

import java.util.Locale;
import java.util.Random;

public class MultiDimensionalArrays {
    public static void main(String[] args) {

        // N th dimension array is an array of (N-1)th dimension arrays  for any integer N > 1
        int[][] matrix = {{1,2,3},{4,5,6,7} ,{7,8,9} ,{10,12} } ;
        int[][] matrix1 = new int[3][4];

        System.out.println("=========== 2D array ======================");
        for(int r=0;r< matrix.length;r++)
        {
            for(int c=0;c< matrix[r].length;c++)
                System.out.printf(Locale.getDefault(),"%-5d,",matrix[r][c]);
            System.out.print("\b\n");
        }

        int count= 0 ;
        System.out.println("=========== 2D array ======================");
        for(int r=0;r< matrix1.length;r++)
        {
            for(int c=0;c< matrix1[r].length;c++) {
                matrix1[r][c] = count++ ;
                System.out.printf(Locale.getDefault(), "%-5d,", matrix1[r][c]);
            }
            System.out.print("\b\n");
        }

        int[][][] ThreeD = new int[3][4][5];
        count = 0 ;
        System.out.println("=========== 3D array ======================");
        for(int d1=0;d1< ThreeD.length;d1++)
        {
            for(int d2=0;d2< ThreeD[d1].length;d2++) {
                for(int d3=0;d3<ThreeD[d1][d2].length;d3++ ) {
                    ThreeD[d1][d2][d3] = count++;
                    System.out.printf(Locale.getDefault(), "%-5d,", ThreeD[d1][d2][d3]);
                }
                System.out.print("\b\n");
            }
            System.out.println("***********************");
        }
        System.out.println("=========== 3D Dynamic array ======================");
        // more than 10 dimensions
        // int[][][][][][][][][][] temp = new int[24][60][60][365][10][10][10][15][180][360] ;

        // 3D random dimension lengths


        count = 0 ;
        Random random = new Random();
        int[][][] Dynamic3D = new int[1+random.nextInt(10)][][];
        for(int i=0;i<Dynamic3D.length;i++)
        {
            Dynamic3D[i] = new int[1+random.nextInt(10)][];
            for(int j=0;j<Dynamic3D[i].length;j++)
            {
                Dynamic3D[i][j] = new int[1+random.nextInt(10)];
                for(int k=0;k<Dynamic3D[i][j].length;k++)
                {
                    Dynamic3D[i][j][k] = count++ ;
                    System.out.printf(Locale.getDefault(),"%4d,",  Dynamic3D[i][j][k]);
                }
                System.out.println();
            }
            System.out.println("*****************************");
        }


        // TODO: create 4D dynamic dimension length array and print all the elements
    }
}

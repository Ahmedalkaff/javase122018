package basics1.completed;

public class Constants {
    public static void main(String[] args) {

        int a = 10 ;    // 10 decimal
        System.out.println("a = " + a);     // 10
        a = 010 ;       // any (integer) number start with 0 is an (octal) number
        System.out.println("a = " + a);     // 8
        a = 0x10 ;      //  // any (integer) number start with 0x/0X is an (Hexadecimal) number
        System.out.println("a = " + a);     // 16
        a = 'A' ;
        System.out.println("a = " + a);     // 16


        char ch= 'A';
        System.out.println("ch = " + ch);   // A
        ch = 65 ;           // the unicode of the letter A (decimal)
        System.out.println("ch = " + ch);   // A
        ch = 0101 ;         // any (integer) number start with 0 is an (octal) number
        System.out.println("ch = " + ch);   // A
        ch = 0x41 ;         // any (integer) number start with 0x/0X is an (Hexadecimal) number
        System.out.println("ch = " + ch);   // A
        ch = '\u0041' ;      // the unicode of the character, unicode must be 4 hexs
        System.out.println("ch = " + ch);   // A

        System.out.println("015.35 = " + 015.35);
        System.out.println("0101 = " + 0101);
        System.out.println("'\\u08BA' = " + '\u08BA');
        System.out.println("ch = \u0041154");

        char bs = 8 ;
        System.out.println("hi"+bs+" bye");


        System.out.println(Integer.toBinaryString(28397));
        System.out.println(067355);
        System.out.println(0X6eed);

        System.out.println("This is Octal :\101");
        System.out.println("This is Hexa :\u0101");

    }
}

package basics1.completed;

public class StringBuilderAndBuffer {

    // since string are immutable so we can't change its value 
    // if we need to build and keep change a value of string use StringBuilder (Async) or StringBuffer (Sync)

    public static void main(String[] args) {

        int[] array = {1, 5, 4, 8, 36, 4, 25, 6, 4, 8, 9, 6, 2, 4, 5};

        // The BAD way
        String str = "{";
        for (int a : array)
            str += a + ", ";

        str = str.substring(0, str.length() - 2) + "}";

        System.out.println("str = " + str);

        // The Good way using StringBuffer
        StringBuffer buffer = new StringBuffer("{");
        for (int a : array)
            buffer.append(a).append(", ");

        str = buffer.substring(0, buffer.length() - 2) + "}";
        System.out.println("str = " + str);

        // The Good way using StringBuilder
        StringBuilder builder = new StringBuilder("{");

        for (int a : array)
            builder.append(a).append(", ");

        str = builder.substring(0, builder.length() - 2) + "}";
        System.out.println("str = " + str);


    }
}

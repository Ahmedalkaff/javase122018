package basics1.completed;

public class MoreInMethods {


    private static String printArray(int[] a) {

        StringBuffer buffer = new StringBuffer("{");
        for(int i=0;i<a.length;i++) {
            buffer.append(a[i]).append(i== a.length-1 ?"}":", ");
        }
        return  buffer.toString();
    }
    void sayHi() {
        System.out.println("Hi");
    }

    static  void staticHi(){
        System.out.println("static hi");
    }

    void addToMe(int a)     // a = y
    {
        a++ ;
        System.out.println("addToMe: a = " + a);
    }

    // All variables in Java are passed to method by value (no pass by reference in java)
    // However, if the variable is of reference type (not primitive) then both variables ( the original and the local)
    // will refer to the same object so any change will effect the object itself


    void addToMe(int[] a)
    {
        for(int i=0;i<a.length;i++)
        a[i]++ ;
        System.out.println("addToMe: a = " + printArray(a));
    }


    static  class AClass {
        int value ;
    }

    void addToMe(AClass c)
    {
        c.value++ ;
        System.out.println("addToMe: a.value = " + c.value);
    }

    void addToMe(String str)
    {
        str = "changed";
        System.out.println("str = " + str);
    }
    public static void main(String[] args) {
        MoreInMethods obj = new MoreInMethods();
        obj.sayHi();

        // for static methods
        staticHi();                 // will work only from any static method inside the same class
        MoreInMethods.staticHi();   // will work from any place if the accessibility is valid

        obj.addToMe(10);

        int y = 20 ;
        System.out.println("before y = " + y);      // 20
        obj.addToMe(y);                             // 21
        System.out.println("after  y = " + y);      // 20
        System.out.println("-------------------");
        int a = 30 ;
        System.out.println("before a = " + a);  // 30
        obj.addToMe(a);                            // 31
        System.out.println("after  a = " + a);      // 30


        int[] arr = {2,5,9};

        System.out.println("Before arr = " + printArray(arr));          //{2, 5, 9}
        obj.addToMe(arr);                                               // {3, 6, 10}
        System.out.println("After arr = " + printArray(arr));          // {3, 6, 10}


        AClass o = new AClass();
        o.value = 50;

        System.out.println("Before o.value = " + o.value);          // 50
        obj.addToMe(o);                                               // 51
        System.out.println("After o.value = " + o.value);          // 51


        String string = "Original";
        System.out.println("Before string = " + string);          // Original
        obj.addToMe(string);                                        // Changed
        System.out.println("After string = " + string);             // Original


    }
}

package basics1.completed;

import java.util.Locale;

public class CastingAndTypeConversions {
    public static void main(String[] args) {


        int a = (int) 53.20;     // casting

        int b = Integer.valueOf("153");

        // radix is the base ( radix 2: binary, 8:ocatal , 10:decimal (default), 16:Hexadecimal

        System.out.printf(Locale.getDefault(), "%-20s OR %-20s%n", Integer.toBinaryString(b), Integer.toString(b, 2));      // 1001 1001
        System.out.printf(Locale.getDefault(), "%-20s OR %-20s%n", Integer.toOctalString(b), Integer.toString(b, 8));
        System.out.printf(Locale.getDefault(), "%-20s OR %-20s%n", b, Integer.toString(b, 10));
        System.out.printf(Locale.getDefault(), "%-20s OR %-20s%n", Integer.toHexString(b), Integer.toString(b, 16));
        System.out.printf(Locale.getDefault(), "Custom radix :%9d ->%-20s%n", 5, Integer.toString(b, 5));


        byte byt = Byte.MAX_VALUE;
        char ch = Character.MIN_VALUE;
        short sho = Short.MIN_VALUE;
        int i = Integer.MAX_VALUE;
        long l = Long.MAX_VALUE;
        float f = Float.MAX_VALUE;
        double d = Double.MIN_VALUE;


        // Error byt = ch ;          // char is 16 bit while byte is 8
        // Error  sho = ch ;          // char is unsigned and its range is from 0-65536 while short is singed and its range is [-32768 to 32767]

        // Error ch = byt ;              // because byte is signed while char is unsigned
        // Error ch = sho ;              // because short is signed while char is unsigned
        // Error ch = i ;                // because int is signed while char is unsigned


        ch = 65;                           // constant value
        // Error  ch = -65 ;                 // char is unsigned

        i = ch;    // integer range is much larger than char range


// Errors because right side is larger than left
//        byt = sho ;     //
//        sho = i ;
//        i = l ;
//        l = f ;
//        f = d ;

        // acceptable automatic casting  byte -> short -> int -> long -> float -> double

        sho = byt;         // 8 bits  into 16 bit

        i = byt;           // 8 bits  into 32 bit
        i = sho;           // 16 bits  into 32 bit

        l = i;           // 32 bits  into 64 bit
        l = sho;         // 16 bits  into 64 bit
        l = byt;          // 8 bits  into 64 bit

        f = l;            // 64 bits  into 32 bit   ????
        f = i;           // 32 bits  into 32 bit   ???
        f = sho;           // 16 bits  into 32 bit
        f = byt;          // 8 bits  into 32 bit

        d = f;           // 32 bits  into 64 bit
        d = l;           // 64 bits  into 64 bit   ??
        d = i;            // 32 bits  into 64 bit
        d = sho;           // 16 bits  into 64 bit
        d = byt;           // 8 bits  into 64 bit

        // Three risky cases are:
        // 1) casting long to float : because long is 8 byte while float is 4
        System.out.println("======1) casting long to float ========");
        // if the value of long is greater than 2^28 (approximately)
        l = Long.MAX_VALUE ;
        f = l ;

        System.out.println("l = " + l);
        System.out.println("f = " + f);

        // 2) casting int to float: because float reserve some places for the floating point value
        System.out.println("======2) casting int to float========");
        // if the value of integer is greater than 2^30 (approximately)

        i = Integer.MAX_VALUE;
        f = i ;
        System.out.println("i = " + i);
        System.out.println("f = " + f);
        // 3) casting long to double  because double reserve some places for the floating point value

        System.out.println("======3) casting long to double ========");
        // if the value of long is greater than 2^55 (approximately)
        l = Integer.MAX_VALUE;
        d = l ;
        System.out.println("l = " + l);
        System.out.println("d = " + d);


        // why they leave this risky cases without handling


    }
}
